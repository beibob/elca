<?php
/**
 * This file is part of the eLCA project
 *
 * eLCA
 * A web based life cycle assessment application
 *
 * Copyright (c) 2023 Online Now! GmbH
 *
 * eLCA is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * eLCA is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with eLCA. If not, see <http://www.gnu.org/licenses/>.
 *
 */
namespace Elca\Db;

use PDO;
use Exception;
use Beibob\Blibs\DbObject;
/**
 *
 * @package elca
 * @author Fabian Möller <fab@beibob.de>
 * @author Tobias Lode <tobias@beibob.de>
 *
 */
class ElcaProjectQNG extends DbObject
{
    /**
     * Tablename
     */
    const TABLE_NAME = 'elca.project_qng_base';

    const QNG_BASE_AMOUNT_VALUE = 40;


    /**
     * projectVariantId
     */
    private $projectVariantId;

    /**
     * QNG Sockel aktiv
     */
    private $projektdataqngactive;

    /**
     * QNG Sockel Grenze  0 / 1
     */
    private $projektdataqngpercent;



    /**
     * Primary key
     */
    private static $primaryKey = array('projectVariantId');

    /**
     * Column types
     */
    private static $columnTypes = array('projectVariantId'      => PDO::PARAM_INT,
                                        'projektdataqngactive'  => PDO::PARAM_BOOL,
                                        'projektdataqngpercent' => PDO::PARAM_INT);

    /**
     * Extended column types
     */
    private static $extColumnTypes = array();


    // public


    /**
     * Creates the object
     *
     * @param  integer $projectVariantId        - projectVariantId
     * @param  bool $projektdataqngactive       
     * @param  integer $projektdataqngpercent   
     * @return ElcaProjectQNG
     */
    public static function create($projectVariantId, $projektdataqngactive = false, $projektdataqngpercent = null)
    {
        $ElcaProjectQNG = new ElcaProjectQNG();
        $ElcaProjectQNG->setProjectVariantId($projectVariantId);
        $ElcaProjectQNG->setProjektdataqngactive($projektdataqngactive);
        $ElcaProjectQNG->setProjektdataqngpercent($projektdataqngpercent);

        if($ElcaProjectQNG->getValidator()->isValid())
            $ElcaProjectQNG->insert();

        return $ElcaProjectQNG;
    }
    // End create



    /**
     * Inits a `ElcaProjectQNG' by its primary key
     *
     * @param  integer  $projectVariantId - projectVariantId
     * @param  boolean  $force           - Bypass caching
     * @return ElcaProjectQNG
     */
    public static function findByProjectVariantId($projectVariantId, $force = false)
    {
        if(!$projectVariantId)
            return new ElcaProjectQNG();

        $sql = sprintf("SELECT project_variant_id
                             , projektdataqngactive
                             , projektdataqngpercent
                             FROM %s
                         WHERE project_variant_id = :projectVariantId"
                       , self::TABLE_NAME
                       );

        return self::findBySql(get_class(), $sql, array('projectVariantId' => $projectVariantId), $force);
    }
    // End findByProjectVariantId



    /**
     * Creates a deep copy from this object
     *
     * @param  int $projectVariantId new project variant id
     * @return Elca - the new element copy
     */
    public function copy($projectVariantId)
    {
        if(!$this->isInitialized() || !$projectVariantId)
            return new ElcaProjectQNG();

        $Copy = self::create($projectVariantId,
                             $this->projektdataqngactive,
                             $this->projektdataqngpercent                             
        );
        return $Copy;
    }
    // End copy


    /**
     * Sets the property projectVariantId
     *
     * @param  integer $projectVariantId - projectVariantId
     * @return void
     */
    public function setProjectVariantId($projectVariantId)
    {
        if(!$this->getValidator()->assertNotEmpty('projectVariantId', $projectVariantId))
            return;

        $this->projectVariantId = (int)$projectVariantId;
    }
    // End setProjectVariantId

    /**
     * Sets the property projektdataqngactive
     *
     * @param  boolean $projektdataqngactive 
     * @return void
     */
    public function setProjektdataqngactive($projektdataqngactive = false)
    {
        $this->projektdataqngactive = (bool)$projektdataqngactive;
    }
    // End setVersion


    /**
     * Sets the property projektdataqngpercent
     *
     * @param  int    $projektdataqngpercent
     * @return void
     */
    public function setProjektdataqngpercent($projektdataqngpercent = 0)
    {
        $this->projektdataqngpercent = (int)$projektdataqngpercent;
    }
    // End setUnitDemand


    /**
     * Returns the property projectVariantId
     *
     * @return integer
     */
    public function getProjectVariantId()
    {
        return $this->projectVariantId;
    }
    // End getProjectVariantId

    /**
     * Returns the property projektdataqngactive
     *
     * @return boolean
     */
    public function getProjektdataqngactive()
    {
        return $this->projektdataqngactive;
    }
    // End projektdataqngactive


    /**
     * Returns the property projektdataqngpercent
     *
     * @return integer
     */
    public function getProjektdataqngpercent()
    {
        return $this->projektdataqngpercent;
    }
    // End getProjektdataqngpercent


    /**
     * Checks, if the object exists
     *
     * @param  integer  $projectVariantId - projectVariantId
     * @param  boolean  $force           - Bypass caching
     * @return boolean
     */
    public static function exists($projectVariantId, $force = false)
    {
        return self::findByProjectVariantId($projectVariantId, $force)->isInitialized();
    }
    // End exists



    /**
     * Updates the object in the table
     *
     * @return boolean
     */
    public function update()
    {
        $sql = sprintf("UPDATE %s
                           SET projektdataqngactive  = :projektdataqngactive
                             , projektdataqngpercent = :projektdataqngpercent
                         WHERE project_variant_id = :projectVariantId"
                       , self::TABLE_NAME
                       );


        return $this->updateBySql($sql,
                                  array('projectVariantId' => $this->projectVariantId,
                                        'projektdataqngactive'   => $this->projektdataqngactive,
                                        'projektdataqngpercent'  => $this->projektdataqngpercent
                                        )
                                  );
    }
    // End update



    /**
     * Deletes the object from the table
     *
     * @return boolean
     */
    public function delete()
    {
        $sql = sprintf("DELETE FROM %s
                              WHERE project_variant_id = :projectVariantId"
                       , self::TABLE_NAME
                      );

        return $this->deleteBySql($sql,
                                  array('projectVariantId' => $this->projectVariantId));
    }
    // End delete



    /**
     * Returns an array with the primary key properties and
     * associates its values, if it's a valid object
     *
     * @param  boolean  $propertiesOnly
     * @return array
     */
    public function getPrimaryKey($propertiesOnly = false)
    {
        if($propertiesOnly)
            return self::$primaryKey;

        $primaryKey = array();

        foreach(self::$primaryKey as $key)
            $primaryKey[$key] = $this->$key;

        return $primaryKey;
    }
    // End getPrimaryKey



    /**
     * Returns the tablename constant. This is used
     * as interface for other objects.
     *
     * @return string
     */
    public static function getTablename()
    {
        return self::TABLE_NAME;
    }
    // End getTablename



    /**
     * Returns the columns with their types. The columns may also return extended columns
     * if the first argument is set to true. To access the type of a single column, specify
     * the column name in the second argument
     *
     * @param  boolean  $extColumns
     * @param  mixed    $column
     * @return mixed
     */
    public static function getColumnTypes($extColumns = false, $column = false)
    {
        $columnTypes = $extColumns? array_merge(self::$columnTypes, self::$extColumnTypes) : self::$columnTypes;

        if($column)
            return $columnTypes[$column];

        return $columnTypes;
    }
    // End getColumnTypes


    // protected


    /**
     * Inserts a new object in the table
     *
     * @return boolean
     */
    protected function insert()
    {
        $sql = sprintf("INSERT INTO %s (project_variant_id, projektdataqngactive, projektdataqngpercent)
                               VALUES  (:projectVariantId, :projektdataqngactive, :projektdataqngpercent)"
            , self::TABLE_NAME
        );

        return $this->insertBySql($sql,
                                  array('projectVariantId' => $this->projectVariantId,
                                        'projektdataqngactive'   => $this->projektdataqngactive,
                                        'projektdataqngpercent'  => $this->projektdataqngpercent
                                   )
        );
    }
    // End insert



    /**
     * Inits the object with row values
     *
     * @param  \stdClass $DO - Data object
     * @return boolean
     */
    protected function initByDataObject(\stdClass $DO = null)
    {
        $this->projectVariantId = (int)$DO->project_variant_id;
        $this->projektdataqngactive              = $DO->projektdataqngactive;
        $this->projektdataqngpercent          = $DO->projektdataqngpercent;


        /**
         * Set extensions
         */
    }
    // End initByDataObject
}
// End class ElcaProjectQNG
BEGIN;
SELECT public.register_patch('20230211-projectconstructions-add-new-field-netRoomSpaceHeated.sql', 'eLCA');

SET search_path = elca;
ALTER TABLE "project_constructions" ADD "net_room_space_heated" integer NULL;

COMMIT;

BEGIN;
SELECT public.register_patch('20230211-projects-add-new-field-assessmentSystem.sql', 'eLCA');

SET search_path = elca;
ALTER TABLE "projects" ADD "assessment_system_id" integer NULL;

COMMIT;

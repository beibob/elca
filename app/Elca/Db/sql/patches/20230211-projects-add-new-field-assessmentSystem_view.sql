BEGIN;
SELECT public.register_patch('20230211-projects-add-new-field-assessmentSystem_view.sql', 'eLCA');

DROP VIEW IF EXISTS elca.projects_view;

CREATE OR REPLACE VIEW elca.projects_view AS
SELECT p.id,
    p.process_db_id,
    p.current_variant_id,
    p.access_group_id,
    p.owner_id,
    p.name,
    p.description,
    p.project_nr,
    p.constr_measure,
    p.life_time,
    p.created,
    p.modified,
    p.constr_class_id,
    p.editor,
    p.is_reference,
    p.benchmark_version_id,
    p.password,
    (array_agg(g.user_id) FILTER (WHERE (g.user_id IS NOT NULL)) || p.owner_id) AS user_ids,
    p.assessment_system_id
   FROM (projects p
     LEFT JOIN public.group_members g ON ((g.group_id = p.access_group_id)))
  GROUP BY 
  p.id, 
  p.process_db_id, 
  p.current_variant_id, 
  p.access_group_id, 
  p.owner_id, 
  p.name, 
  p.description, 
  p.project_nr, 
  p.constr_measure, 
  p.life_time, 
  p.created, 
  p.modified, 
  p.constr_class_id, 
  p.editor, 
  p.is_reference, 
  p.benchmark_version_id, 
  p.password,
  p.assessment_system_id
;
COMMIT;

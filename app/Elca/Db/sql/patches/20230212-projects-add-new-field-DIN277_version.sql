BEGIN;
SELECT public.register_patch('20230212-projects-add-new-field-DIN277_version.sql', 'eLCA');

SET search_path = elca;
ALTER TABLE "projects" ADD "din277_version" integer NULL;

COMMIT;

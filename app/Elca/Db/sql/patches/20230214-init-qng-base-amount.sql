BEGIN;
SELECT public.register_patch('20230214-init-qng-base', 'elca');

CREATE TABLE elca.project_qng_base
(
      "project_variant_id"      int       -- projectVariantId
    , "projektdataqngactive"    boolean    NOT NULL DEFAULT false       -- qng base active
    , "projektdataqngpercent"   smallint   NOT NULL DEFAULT 0           -- qng base percent 0 - 1 
    , PRIMARY KEY ("project_variant_id")
    , FOREIGN KEY ("project_variant_id") REFERENCES elca.project_variants ("id") ON UPDATE CASCADE ON DELETE CASCADE
);



COMMIT;
BEGIN;
SELECT public.register_patch('20230214-projectenev-add-new-field-refspacenrf.sql', 'eLCA');

SET search_path = elca;
ALTER TABLE "project_en_ev" ADD "refspacenrf" numeric NULL;

COMMIT;
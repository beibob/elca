BEGIN;
SELECT public.register_patch('20230306-projectenev-add-new-field-pvselfused.sql', 'eLCA');

SET search_path = elca;
ALTER TABLE "project_en_ev" ADD "pvselfused" numeric NULL;

COMMIT;
BEGIN;
SELECT public.register_patch('20230314-element_component-add-new-field-selfusedelectricitypercent.sql', 'eLCA');

SET search_path = elca;
ALTER TABLE "element_components" ADD "selfusedelectricitypercent" numeric NULL;

COMMIT;
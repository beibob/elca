BEGIN;
SELECT public.register_patch('20230314-extend-element_components_v-selfusedelectricitypercent.sql', 'elca');

DROP VIEW IF EXISTS elca.element_components_v;


CREATE VIEW elca.element_components_v AS
SELECT c.id,
    c.element_id,
    c.process_config_id,
    c.quantity,
    c.process_conversion_id,
    c.life_time,
    c.calc_lca,
    c.is_layer,
    c.layer_position,
    c.layer_size,
    c.layer_sibling_id,
    c.layer_area_ratio,
    c.created,
    c.modified,
    c.layer_length,
    c.layer_width,
    c.is_extant,
    c.life_time_delay,
    c.life_time_info,
    e.name AS element_name,
    c.selfusedelectricitypercent
   FROM (element_components c
     JOIN elements e ON ((e.id = c.element_id)));

COMMIT;
BEGIN;
SELECT public.register_patch('add-is-compliant-a2-process-dbs', 'elca');

ALTER TABLE elca.process_dbs ADD "is_a2_compliant" BOOLEAN NOT NULL DEFAULT FALSE;

UPDATE elca.process_dbs
SET is_a2_compliant = false;

COMMIT; 
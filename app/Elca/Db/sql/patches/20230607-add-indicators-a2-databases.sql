BEGIN;
SELECT public.register_patch('add-indicators-a2-databases', 'elca');

INSERT INTO elca.indicators (id, name, ident, unit, is_excluded, p_order, description, uuid, is_en15804_compliant, is_a2_compliant)
   VALUES (35,'GWP total','gwpTotal','kg CO2-Äqv.',false,400,'Globales Erwärmungspotenzial - total (GWP-total)','6a37f984-a4b3-458a-a20a-64418c145fa2',true,true),
    (36,'GWP-biogenic','gwpBiogenic','kg CO2-Äqv.',false,410,'Globales Erwärmungspotenzial - biogen','2356e1ab-0185-4db5-86e5-16de51c7485c',true,true),
    (37,'GWP-fossil','gwpFossil','kg CO2-Äqv.',false,420,'Globales Erwärmungspotenzial - fossil','5f635281-343e-44fb-83df-1971b155e6b6',true,true),
    (38,'GWP-luluc','gwpLuluc','kg CO2-Äqv.',false,430,'Globales Erwärmungspotenzial - luluc','4331bbdb-978a-490d-8707-eeb047f01a55',true,true),
    (39, 'ETP-fw','etpFw','CTUe',false,440,'Potenzielle Toxizitätsvergleichseinheit für Ökosysteme','ee1082d1-b0f7-43ca-a1f0-21e2a4a74511',true,true),
    (40,'PM','pm','Auftreten von Krankheiten',false,450,'Potenzielles Auftreten von Krankheiten aufgrund von Feinstaubemissionen','b5c602c6-def3-11e6-bf01-fe55135034f3',true,true),
    (41,'EP-marine','epMarine','kg N-Äq.',false,460,'Eutrophierungspotenzial - Salzwasser','b5c619fa-def3-11e6-bf01-fe55135034f3',true,true),
    (42,'EP-freshwater','epFreshwater','kg P-Äqv.',false,470,'Eutrophierungspotenzial - Süßwasser','b53ec18f-7377-4ad3-86eb-cc3f4f276b2b',true,true),
    (43,'EP-terrestrial','epTerrestrial','mol N-Äq.',false,480,'Eutrophierungspotenzial - Land ','b5c614d2-def3-11e6-bf01-fe55135034f3',true,true),
    (44,'HTP-c','htpC','CTUh',false,490,'Potenzielle Toxizitätsvergleichseinheit für den Menschen - kanzerogene Wirkung','2299222a-bbd8-474f-9d4f-4dd1f18aea7c',true,true),
    (45,'HTP-nc','htpNc','CTUh',false,500,'Potenzielle Toxizitätsvergleichseinheit für den Menschen - nicht kanzerogene Wirkung','3af763a5-b7a1-48c9-9cee-1f223481fcef',true,true),
    (46,'IRP','irp','kBq U235-Äqv.',false,510,'Potenzielle Wirkung durch Exposition des Menschen mit U235','b5c632be-def3-11e6-bf01-fe55135034f3',true,true),
    (47,'SQP','sqp','dimensionslos',false,520,'Potenzieller Bodenqualitätsindex','b2ad6890-c78d-11e6-9d9d-cec0c932ce01',true,true),
    (48,'POCP','POCP','kg NMVOC-Äqv.',false,530,'Bildungspotenzial für troposphärisches Ozon','b5c610fe-def3-11e6-bf01-fe55135034f3',true,true),
    (49,'WDP','wdp','m3 Welt-Äqv.',false,540,'Wasser-Entzugspotenzial (Benutzer)','b2ad66ce-c78d-11e6-9d9d-cec0c932ce01',true,true)
;

COMMIT;
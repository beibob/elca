BEGIN;
SELECT public.register_patch('20230718-alter-indicators-a2-databases_porderA2_value', 'elca');

UPDATE elca.indicators set p_ordera2 = 10, is_hidden_a2 = FALSE where id = 19;
UPDATE elca.indicators set p_ordera2 = 20, is_hidden_a2 = FALSE where id = 18;
UPDATE elca.indicators set p_ordera2 = 30, is_hidden_a2 = FALSE where id = 17;
UPDATE elca.indicators set p_ordera2 = 40, is_hidden_a2 = FALSE where id = 16;
UPDATE elca.indicators set p_ordera2 = 50, is_hidden_a2 = FALSE where id = 15;
UPDATE elca.indicators set p_ordera2 = 60, is_hidden_a2 = FALSE where id = 14;
UPDATE elca.indicators set p_ordera2 = 70, is_hidden_a2 = FALSE where id = 31;
UPDATE elca.indicators set p_ordera2 = 80, is_hidden_a2 = FALSE where id = 32;
UPDATE elca.indicators set p_ordera2 = 90, is_hidden_a2 = FALSE where id = 35;
UPDATE elca.indicators set p_ordera2 = 100, is_hidden_a2 = FALSE where id = 36;
UPDATE elca.indicators set p_ordera2 = 110, is_hidden_a2 = FALSE where id = 37;
UPDATE elca.indicators set p_ordera2 = 120, is_hidden_a2 = FALSE where id = 38;
UPDATE elca.indicators set p_ordera2 = 130, is_hidden_a2 = FALSE where id = 10;
UPDATE elca.indicators set p_ordera2 = 140, is_hidden_a2 = FALSE where id = 13;
UPDATE elca.indicators set p_ordera2 = 150, is_hidden_a2 = FALSE where id = 11;
UPDATE elca.indicators set p_ordera2 = 160, is_hidden_a2 = FALSE where id = 41;
UPDATE elca.indicators set p_ordera2 = 170, is_hidden_a2 = FALSE where id = 43;
UPDATE elca.indicators set p_ordera2 = 180, is_hidden_a2 = FALSE where id = 42;
UPDATE elca.indicators set p_ordera2 = 190, is_hidden_a2 = FALSE where id = 23;
UPDATE elca.indicators set p_ordera2 = 200, is_hidden_a2 = FALSE where id = 49;
UPDATE elca.indicators set p_ordera2 = 210, is_hidden_a2 = FALSE where id = 25;

COMMIT;
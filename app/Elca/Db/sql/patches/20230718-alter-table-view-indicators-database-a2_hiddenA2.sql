BEGIN;
SELECT public.register_patch('20230718-alter-table-view-indicators-database-a2_hiddenA2', 'elca');

ALTER TABLE elca.indicators ADD "is_hidden_a2" BOOLEAN NOT NULL DEFAULT TRUE;

UPDATE elca.indicators
SET is_hidden_a2 = TRUE;

DROP MATERIALIZED VIEW IF EXISTS elca.indicators_v;
CREATE MATERIALIZED VIEW elca.indicators_v AS
    SELECT DISTINCT i.*
         , p.process_db_id
    FROM elca.indicators i
    JOIN elca.process_indicators pi ON i.id = pi.indicator_id
    JOIN elca.processes p ON p.id = pi.process_id;

COMMIT; 


BEGIN;
SELECT public.register_patch('20230718-alter-table-view-indicators-database-a2_porderA2', 'elca');

ALTER TABLE elca.indicators ADD "p_ordera2" SMALLINT DEFAULT NULL;

UPDATE elca.indicators
SET p_ordera2 = NULL;

DROP MATERIALIZED VIEW IF EXISTS elca.indicators_v;
CREATE MATERIALIZED VIEW elca.indicators_v AS
    SELECT DISTINCT i.*
         , p.process_db_id
    FROM elca.indicators i
    JOIN elca.process_indicators pi ON i.id = pi.indicator_id
    JOIN elca.processes p ON p.id = pi.process_id;

COMMIT; 


BEGIN;
SELECT public.register_patch('20230720-1-cache-porder-a2-indicators_report-total-effects.sql', 'eLCA');

DROP VIEW IF EXISTS elca_cache.report_total_effects_v;
CREATE VIEW elca_cache.report_total_effects_v AS
    SELECT ci.item_id
        , ci.indicator_id
        , ci.value
        , i.name AS name
        , i.ident AS ident
        , i.unit AS unit
        , i.is_hidden
        , (CASE WHEN (NOT i.is_hidden_a2) THEN i.p_ordera2 ELSE i.p_order END)  AS indicator_p_order
        , i.p_order
        , v.project_variant_id
        , 'Gesamt'::varchar AS category
    FROM elca_cache.project_variants v
        JOIN elca_cache.indicators ci ON ci.item_id = v.item_id
        JOIN elca.indicators i ON i.id = ci.indicator_id
        JOIN elca.life_cycles l ON l.ident = ci.life_cycle_ident
    WHERE ci.life_cycle_ident = 'total';


COMMIT;
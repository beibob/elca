BEGIN;
SELECT public.register_patch('20230726-add-update-indicators-ADP_a2-databases.sql', 'elca');

UPDATE  "indicators" set p_ordera2 = NULL, is_hidden_a2='t' where ident = 'adpe' AND is_a2_compliant='f';
UPDATE  "indicators" set p_ordera2 = NULL, is_hidden_a2='t' where ident = 'adpf' AND is_a2_compliant='f';

INSERT INTO "indicators" ("id", "name", "ident", "unit", "is_excluded", "p_order", "description", "uuid", "is_en15804_compliant", "is_hidden", "is_a2_compliant", "is_hidden_a2", "p_ordera2") 
VALUES
(52,	'ADP elem.',	'adpe',	'kg Sb-Äqv.',	'f',	134,	'ADP elementar',	'b2ad6494-c78d-11e6-9d9d-cec0c932ce01',	't',	'f',	't',	'f',	70),
(53,	'ADP fossil',	'adpf',	'MJ',	'f',	148,	'ADP fossil',	'b2ad6110-c78d-11e6-9d9d-cec0c932ce01',	't',	'f',	't',	'f',	80);

COMMIT;
BEGIN;
SELECT public.register_patch('20230727-add-p_ordera2_cache-indicator-results-v.sql', 'elca');

DROP VIEW IF EXISTS elca_cache.indicator_results_v;
CREATE VIEW elca_cache.indicator_results_v AS
    SELECT ci.item_id
        , ci.life_cycle_ident
        , ci.indicator_id
        , ci.process_id
        , ci.value
        , ci.ratio
        , ci.is_partial
        , p.name_orig
        , l.name AS life_cycle_name
        , l.phase AS life_cycle_phase
        , l.p_order AS life_cycle_p_order
        , i.name AS indicator_name
        , i.ident AS indicator_ident
        , i.is_hidden
        , i.p_order AS indicator_p_order
        , i.p_ordera2 AS p_ordera2
    FROM elca_cache.indicators ci
        JOIN elca.indicators i ON i.id = ci.indicator_id
        JOIN elca.life_cycles l ON ci.life_cycle_ident = l.ident
        LEFT JOIN elca.processes p ON ci.process_id = p.id;


COMMIT;
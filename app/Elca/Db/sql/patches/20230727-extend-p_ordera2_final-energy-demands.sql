BEGIN;
SELECT public.register_patch('20230727-extend-p_ordera2_final-energy-demands.sql', 'elca');

DROP VIEW IF EXISTS elca_cache.report_final_energy_demand_effects_v;

CREATE VIEW elca_cache.report_final_energy_demand_effects_v AS
SELECT f.id
        , f.project_variant_id
        , f.ident
        , f.ratio
        , f.kwk_id
        , cf.quantity AS element_quantity
        , cf.ref_unit AS element_ref_unit
        , pc.name AS element_name
        , ci.indicator_id AS indicator_id
        , ci.value AS indicator_value
        , i.name AS indicator_name
        , i.unit AS indicator_unit
        , i.is_hidden
        , i.p_order AS indicator_p_order
        , i.p_ordera2 AS p_ordera2
FROM elca.project_final_energy_demands f
         JOIN elca.process_configs              pc ON pc.id = f.process_config_id
         JOIN elca_cache.final_energy_demands_v cf ON f.id = cf.final_energy_demand_id
         JOIN elca_cache.indicators             ci ON cf.item_id = ci.item_id AND ci.life_cycle_ident = 'total'
         JOIN elca.indicators                    i ON i.id = ci.indicator_id;

COMMIT;
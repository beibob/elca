BEGIN;
SELECT public.register_patch('20230824-cache-porder-fix-indicators_report_live_cycle_effects.sql', 'eLCA');

DROP VIEW IF EXISTS elca_cache.report_total_energy_recycling_potential;
DROP VIEW IF EXISTS elca_cache.report_life_cycle_effects_v;
CREATE VIEW elca_cache.report_life_cycle_effects_v AS
    SELECT cv.project_variant_id
        , ci.item_id
        , ci.indicator_id
        , ci.value
        , i.name AS name
        , i.ident AS ident
        , i.unit AS unit
        , i.is_hidden
        , (CASE WHEN (i.is_a2_compliant) THEN i.p_ordera2 ELSE i.p_order END)  AS indicator_p_order
        , l.name AS category
        , l.ident AS life_cycle_ident
        , l.phase AS life_cycle_phase
        , l.p_order AS life_cycle_p_order
        , i.p_ordera2 AS p_ordera2 
    FROM elca_cache.project_variants cv
        JOIN elca_cache.indicators ci ON ci.item_id = cv.item_id
        JOIN elca.indicators i ON i.id = ci.indicator_id
        JOIN elca.life_cycles l ON ci.life_cycle_ident = l.ident
    WHERE ci.is_partial = false;



CREATE VIEW elca_cache.report_total_energy_recycling_potential AS
    SELECT null::int AS item_id
        , t.indicator_id
        , t.name
        , t.ident
        , t.unit
        , t.is_hidden
        , t.indicator_p_order
        , t.project_variant_id
        , 'D energetisch'::varchar AS category
        , r.life_cycle_p_order
        , r.life_cycle_ident
        , t.value - r.value AS value
    FROM elca_cache.report_life_cycle_effects_v t
    JOIN elca_cache.report_total_construction_recycling_effects_v r ON t.project_variant_id = r.project_variant_id
                                                                       AND t.indicator_id = r.indicator_id
   WHERE t.life_cycle_phase = 'rec';


COMMIT;
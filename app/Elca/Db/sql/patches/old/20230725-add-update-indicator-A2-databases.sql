BEGIN;
SELECT public.register_patch('20230725-add-update-indicator-A2-databases.sql', 'elca');

INSERT INTO elca.indicators (id, name, ident, unit, is_excluded, p_order, description, uuid, is_en15804_compliant, is_hidden, is_a2_compliant, is_hidden_a2, p_ordera2)
VALUES 
(50,	'AP',	'ap',	'mol H+ eqv.',	'f',	40,	'AP',	'b5c611c6-def3-11e6-bf01-fe55135034f3',	't',	'f',	't',	'f',	130),
(51,	'ODP',	'odp',	'kg CFC 11-Äquiv.',	'f',	20,	'ODP',	'b5c629d6-def3-11e6-bf01-fe55135034f3',	't',	'f',	't',	'f',	140);

UPDATE elca.indicators
   SET p_ordera2 = 190, is_hidden='f', is_hidden_a2='f'
 WHERE ident = 'fw';

UPDATE elca.indicators
   SET is_hidden_a2 = 't'
 WHERE ident = 'ap' AND uuid='b4274add-93b7-4905-a5e4-2e878c4e4216';

UPDATE elca.indicators
   SET is_hidden_a2 = 't'
 WHERE ident = 'pocp' AND uuid='1e84a202-dae6-42aa-9e9d-71ea48b8be00';

UPDATE elca.indicators
   SET is_hidden_a2 = 't'
 WHERE ident = 'odp' AND uuid='06dcd26f-025f-401a-a7c1-5e457eb54637';

UPDATE elca.indicators
   SET is_hidden_a2 = 't'
 WHERE ident = 'hwd' AND uuid='430f9e0f-59b2-46a0-8e0d-55e0e84948fc';

UPDATE elca.indicators
   SET is_hidden_a2 = 'f'
 WHERE ident = 'POCP' AND uuid='b5c610fe-def3-11e6-bf01-fe55135034f3';

COMMIT;
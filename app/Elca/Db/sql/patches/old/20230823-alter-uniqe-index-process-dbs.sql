BEGIN;
SELECT
    public.register_patch('20230823-alter-uniqe-index-process-dbs.sql', 'eLCA');

ALTER TABLE elca.process_dbs DROP CONSTRAINT process_dbs_uuid_key;
CREATE UNIQUE INDEX process_dbs_uuid_is_a2_compliant ON elca.process_dbs (uuid,is_a2_compliant);

COMMIT;


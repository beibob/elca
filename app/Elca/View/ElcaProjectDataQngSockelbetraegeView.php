<?php
/**
 * This file is part of the eLCA project
 *
 * eLCA
 *
 * Copyright (c) 2010-2011 Tobias Lode <tobias@beibob.de>
 *               BEIBOB Medienfreunde GbR - http://beibob.de/
 *               Created/Modified: Online Now! 2022/2023   
 * Licensed under Creative Commons license CC BY-NC 3.0
 * http://creativecommons.org/licenses/by-nc/3.0/de/
 */
namespace Elca\View;

use Beibob\Blibs\FrontController;
use Beibob\Blibs\HtmlView;
use Beibob\HtmlTools\HtmlForm;
use Beibob\HtmlTools\HtmlFormGroup;
use Beibob\HtmlTools\HtmlRadioGroup;
use Beibob\HtmlTools\HtmlRadiobox;
use Beibob\HtmlTools\HtmlHiddenField;
use Beibob\HtmlTools\HtmlCheckbox;
use Beibob\HtmlTools\HtmlTag;
use Beibob\HtmlTools\HtmlTable;
use Beibob\HtmlTools\HtmlTableHeadRow;
use Elca\Db\ElcaIndicatorSet;
use Elca\View\helpers\ElcaHtmlFormElementLabel;
use Elca\View\helpers\ElcaHtmlNumericInput;
use Elca\View\helpers\ElcaHtmlSubmitButton;

/**
 * Builds the project data benchmarks view
 *
 * @package    elca
 * @author     Michael Boeneke
 * @copyright  Online Now! GmbH
 */
class ElcaProjectDataQngSockelbetraegeView extends HtmlView
{
    /**
     * ProjectId
     */
    private $projectId;

    private $projektdataqngactive;
    private $projektdataqngpercent;
    
    /**
     * Data
     */
    private $Data;

    private $readOnly;
    
    private $sockelbetraginfos = [
        '410' => 'Steig- und Fallrohrleitungen, Anschlussleitungen für Wohnungen und alle Sanitärobjekte',
        '420' => 'Rohrleitungen, Verteiler für Raumheizflächen, Raumheizflächen',
        '430' => 'Rohrleitungen, Verteiler, Anschlussleitungen Lüftung',
        '440' => 'Niederspannungshauptverteiler, Kabel, Leitungen, Unterverteiler',
        '450' => 'Leerrohre, Kabel, Leitungen, Personenrufanagen, Lichtruf- und Klingelanlagen, Türsprech- und Türöffneranlagen'
    ];

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Init
     *
     * @param  array $args
     * @return -
     */
    protected function init(array $args = [])
    {
        parent::init($args);

        /**
         * Init arguments and options
         */
         
        $this->projectVariantId = $this->get('projectVariantId');
        $this->projektdataqngactive = $this->get('projektdataqngactive');
        $this->projektdataqngpercent = $this->get('projektdataqngpercent');
        // var_dump((bool)$this->projektdataqngactive);
        
        
        $this->readOnly = $this->get('readOnly');
    }
    // End init

    //////////////////////////////////////////////////////////////////////////////////////

    /**
     * Callback triggered after rendering the template
     */
    protected function afterRender()
    {
        /**
         * Add Container
         */
        $Container = $this->appendChild($this->getDiv(['id' => 'content',
                                                            'class' => 'project-qng-base']));
    
        $formId = 'projectDataQngBaseAmountsForm';
        $Form = new HtmlForm($formId, '/project-data/saveQngBaseAmounts/');
        $Form->setAttribute('id', $formId);
        $Form->addClass('clearfix highlight-changes');
        // $Form->setReadonly($this->readOnly);

        //$Form->setDataObject($this->Data);

        if($this->has('Validator'))
        {
            $Form->setValidator($this->get('Validator'));
            $Form->setRequest(FrontController::getInstance()->getRequest());
        }

        /**
         * Add hidden projectVariantId
         */
        $Form->add(new HtmlHiddenField('projectVariantId', $this->projectVariantId));

        $Group = $Form->add(new HtmlFormGroup(t('Sockelbeträge QNG')));
        
        $label = $Group->add(
            new ElcaHtmlFormElementLabel(
                t('QNG Sockelbeträge aktiv')
            )
        );
        $label->addClass("qngsockelaktiv");
        $label->add(
            new HtmlCheckbox(
                'projektdataqngactive',
                $this->projektdataqngactive, 
                '',
                false
            )
        );
        // $Group->add(new ElcaHtmlFormElementLabel(t('Sockelbeträge QNG'), new HtmlCheckbox('projektdataqngactive')));
        
        // $contentchange = $Form->add(new HtmlTag('div'));        
        // $contentchange->setAttribute('id','qngsockeltcontent');
        // $contentchange->setAttribute('class','project-qng-base-content');

        
        $TableHinweis = $Form->add( new HtmlTag('table'));
        $TableHinweis->setAttribute('class','infoboxrahmen sockelbetraginfobox' );
        $TableHinweisHead = $TableHinweis->add( new HtmlTag('thead'));
        $TableHinweisHead->add( new HtmlTag('th',t('Kostengruppe')));
        $TableHinweisHead->add( new HtmlTag('th',t('Im Sockelbetrag pauschal erfasste Bauteile')));
        $TableHinweisBody = $TableHinweis->add( new HtmlTag('tbody'));
        
        foreach($this->sockelbetraginfos as $this->sockelbetraginfokey => $this->sockelbetraginfovalue)
        {
            $Row = $TableHinweisBody->add( new HtmlTag('tr'));
            $Row->add( new HtmlTag('td',$this->sockelbetraginfokey));
            $Row->add( new HtmlTag('td',$this->sockelbetraginfovalue));
        }
        
        $Radio = $Form->add( new ElcaHtmlFormElementLabel(t('Qp in %'), $RadioGroup = new HtmlRadioGroup('projektdataqngpercent',$this->projektdataqngpercent)));
        $Radio->setAttribute('class','infoboxrahmen clearfix' );
        
        $RadioSelect = $RadioGroup->add(new HtmlRadiobox(t('> 40'), 0));
        if (isset($this->projektdataqngpercent) && $this->projektdataqngpercent == 0)
            $RadioSelect->setAttribute('selected', 'selected');
        
        $RadioSelect = $RadioGroup->add(new HtmlRadiobox(t('<= 40'), 1));
        if (isset($this->projektdataqngpercent) && $this->projektdataqngpercent == 1)
            $RadioSelect->setAttribute('selected', 'selected');
        
        
        $ButtonGroup = $Form->add(new HtmlFormGroup(''));
        $ButtonGroup->addClass('buttons');
        $ButtonGroup->add(new ElcaHtmlSubmitButton('saveBaseAmount', t('Speichern'), true));
        
        
        $qngnotice = $Form->add(new HtmlTag('p',t('Nicht alle Anlagenteile der Kostengruppe 400 werden durch die Sockelbeträge berücksichtigt. Die Bilanzgrößen ausgewählter Anlagenteile werden durch Einzelerfassung unter Nutzung der Datenbank ÖKOBAUDAT_2020_II ermittelt. Siehe "Anhangdokument 3.1.1 LCA-Bilanzregeln Wohngebäude" unter https://www.nachhaltigesbauen.de/austausch/beg/.')));
        $qngnotice->setAttribute('class','project-qng-base-content-notice hidden');           
        
        $Form->appendTo($Container);
        
         
        
    }
    // End afterRender

    //////////////////////////////////////////////////////////////////////////////////////




    //////////////////////////////////////////////////////////////////////////////////////
}
// End ElcaProjectDataQngSockelbetraegeView

<div id="overlayhinweis">
<h2><strong>Ankündigung Wartungsarbeiten</strong></h2>
<p>Sehr geehrte Damen und Herren,<br/><br/>

ab <span class="markiertfett">Freitag, 18.08.2023</span> finden umfangreiche <span class="markiertfett">Wartungsarbeiten</span> auf der Plattform "eLCA Bauteileditor" statt.<br/><br/>
Zur Vorbereitung ist bereits ab <span class="markiertfett">Donnerstag, 17.08.2023, 18 Uhr</span> <span class="markiertfettunderline">keine Anmeldung und keine Bearbeitung</span> mehr möglich.<br/><br/>

Geplant ist, diese Wartungsarbeiten am 18.08.2023 abzuschließen. <br/>
Bei Problemen bleibt der Wartungsmodus für Korrekturen bis zum Montag, 21.08.2023 bestehen.<br/><br/>

Bei Fragen wenden Sie sich bitte an das <a href="/imprint/" style="font-size:16px">BBSR Referat</a>.
</p>
<div class="closebtnoverlay">
    <button class="button">Schließen</button>
</div>
</div>
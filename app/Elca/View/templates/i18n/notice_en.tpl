<div id="overlayhinweis">
<h2><strong>Announcement of maintenance work</strong></h2>
<p>Dear Ladies and Gentlemen,<br/><br/>

from <strong>Friday, 18.08.2023</strong>, extensive <strong>maintenance work</strong> will take place on the platform "eLCA Bauteileditor".<br/><br/>
In preparation for this, <strong><u>registration and editing</u> will no longer be possible from 6 p.m. on Thursday, 17.08.2023</strong>.<br/><br/>

It is planned to complete this maintenance work on 18.08.2023. <br/>
In case of problems, the maintenance mode will remain in place for corrections until Monday, 21.08.2023.<br/><br/>

If you have any questions, please contact the  <a href="/imprint/" style="font-size:16px">BBSR unit</a>.
</p>
<div class="closebtnoverlay">
    <button class="button">Close</button>
</div>
</div>










.
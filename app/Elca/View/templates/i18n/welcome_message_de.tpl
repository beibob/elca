<h1>Willkommen, $$username$$.</h1>

<p style="font-size:14px;line-height:20px;">Mit eLCA haben Sie die Möglichkeit Ökobilanzen für
    Gebäudemodelle, auf der Grundlage vorhandener und
    individueller Bauteilvorlagen, zu erstellen.<br/><br/>
    Bitte beachten Sie auch unsere <a href="/information"><strong>Informationen zu eLCA</strong></a></p>

<p style="font-size:14px;line-height:20px;">Die Dateneingabe erfolgt projektbasiert auf der Grundlage eines
    Gebäudemodells, das in seiner Baukonstruktion durch Bauteile und
    Bauteilkomponenten mit den dazugehörigen Materialien und für den
    Gebäudebetrieb durch die eingesetzten Energieträger beschrieben
    werden kann. Die Auswertung für ein Projekt wird Ihnen als Massenbilanz
    und Wirkungseinschätzung für alle verwendeten Bauteile und für
    eine Gesamtbetrachtung nach Lebenszyklusphasen, Bauteilgruppen und
    dem Gesamtergebnis bereits bei der Dateneingabe BNB-konform angeboten.</p>

<p style="font-size:14px;line-height:20px;">Den in der Anwendung angebotenen Materialien wurden die
    ökologischen Parameter der vom BBSR bereitgestellten
    Baustoff-Datenbanken Ökobau.dat in den Versionen 2009, 2011 und
    2013 über den Lebenszyklus zugeordnet.</p>

<p style="font-size:14px;line-height:20px;">Sie haben nun die Möglichkeit, unter <a href="/projects/"><strong>Projekte</strong></a> ein neues Projekt anzulegen
    oder ein bereits vorhandenes zu bearbeiten.</p>

<p style="font-size:14px;line-height:20px;">Oder Sie können unter <a href="/elements/"><strong>Bauteilvorlagen</strong></a> öffentliche
    Bauteilvorlagen einsehen, und diese nach eigenen
    Kriterien individuell anpassen, um Sie später in Ihren beispiel Projekten zu verwenden.</p>

<p style="font-size:14px;line-height:20px;">Unter <a href="/processes/"><strong>Baustoffe</strong></a> erhalten Sie einen
    Überblick über die in der Anwendung hinterlegten Baustoffe und
    ihrer ökologischen Parameter.</p>

